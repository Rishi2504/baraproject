import { BaraprojectPage } from './app.po';

describe('baraproject App', () => {
  let page: BaraprojectPage;

  beforeEach(() => {
    page = new BaraprojectPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
