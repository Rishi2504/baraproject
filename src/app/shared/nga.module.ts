import { NgModule, ModuleWithProviders ,  NO_ERRORS_SCHEMA} from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import {
} from './services';
const NGA_COMPONENTS = [
  
];
const NGA_SERVICES = [
];
@NgModule({
  declarations: [
    ...NGA_COMPONENTS,
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,   
  ],
  schemas: [ NO_ERRORS_SCHEMA ],
  exports: [
    ...NGA_COMPONENTS,
  ],
})
export class NgaModule {
  static forRoot(): ModuleWithProviders {
    return <ModuleWithProviders> {
      ngModule: NgaModule,
      providers: [
        ...NGA_SERVICES,
      ],
    };
  }
}
