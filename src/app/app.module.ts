import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { Http, HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { AuthGuard } from './shared';
import { NgaModule } from './shared/nga.module';
import { Approuting } from './app.routes';
import { BaraprojectModule } from './baraproject/baraproject.module';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    Approuting,
    FormsModule,
    RouterModule,
    HttpModule,
    BaraprojectModule,
    NgaModule.forRoot(),
  ],
  providers: [AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
