import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import * as _ from 'underscore';

@Injectable()
export class GlobalService {
    constructor(private http:Http) {
    }

    public extractData(res: Response) {
    	let body = res.json();
        //console.log("Global Service called", body);
        if( body.hasOwnProperty('error') ) {
            if(body.error.message === 'Token is required') {
                localStorage.removeItem('isLoggedin');
                localStorage.removeItem('currentUser');
                localStorage.removeItem('token');
                document.cookie = "token="+'';
                // window.location.href = '/';
            } else {
                return Promise.resolve(body || {});
            }
        } else {
            return Promise.resolve(body || {});
        }
    }

    public handleErrorPromise (error: Response | any) {
        let body = error.json();
        console.log("Global Error Service called", error);
        if(error.status === 400) {
            return Promise.reject(body.error || error);
        } else {
            localStorage.removeItem('isLoggedin');
            localStorage.removeItem('currentUser');
            localStorage.removeItem('token');
            document.cookie = "token="+'';
            // window.location.href = '/';
        }
    }
}
