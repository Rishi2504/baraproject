import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import { GlobalService } from '../global.service';

@Injectable()
export class GetAccount {
    
    constructor(private http:Http, private globalService: GlobalService) {       
    }

    getDetail(username, password): Promise<any> {
        let url = 'http://www.baraproject.nl/api/Account/';
        
        return this.http.get(url).toPromise()
		    .then(this.globalService.extractData)
	            .catch(this.globalService.handleErrorPromise);
    }
}
