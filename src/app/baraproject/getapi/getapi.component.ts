import { Component, OnInit } from '@angular/core';
import { GetAccount } from './getapi.service';
@Component({
  selector: 'app-getapi',
  templateUrl: './getapi.component.html',
  styleUrls: ['./getapi.component.css']
})
export class GetapiComponent implements OnInit {
account:any = {};
  constructor(private getAccount:GetAccount) { }

  ngOnInit() {
    this.getAccountDetail()
  }

  getAccountDetail() { 
        let username = "jane@bara.nl";
        let password = "welkom123";     
        this.getAccount.getDetail(username, password).then(data => {            
            this.account = data.data;
            console.log("account", this.account)
        }).catch(error => {
            console.log("Account detail error ", error);
        });
    }

}
