import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { routing } from './getapi.routes';
import { GetapiComponent } from './getapi.component';
@NgModule({
  imports: [
    CommonModule,
    routing
  ],
  declarations: [GetapiComponent]
})
export class GetapiModule { }
