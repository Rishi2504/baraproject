import { Routes, RouterModule }  from '@angular/router';

import { GetapiComponent } from './getapi.component';
import { ModuleWithProviders } from '@angular/core';

export const routes: Routes = [
    {
        path: '',
        component: GetapiComponent,
        children: [
        ]
    }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
