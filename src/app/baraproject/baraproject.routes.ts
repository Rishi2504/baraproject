import { Routes, RouterModule }  from '@angular/router';
import { Baraproject } from './baraproject.component';
import { ModuleWithProviders } from '@angular/core';
// noinspection TypeScriptValidateTypes

// export function loadChildren(path) { return System.import(path); };

export const routes: Routes = [
    {
        path: '',
        component: Baraproject,
        children: [
            { path: '', loadChildren: './getapi/getapi.module#GetapiModule' },
            { path: 'getapi', loadChildren: './getapi/getapi.module#GetapiModule' },
            
        ],
        canActivate: []
    },
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
