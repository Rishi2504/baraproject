import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { routing } from './baraproject.routes';
import { Baraproject } from './baraproject.component';
import { GetAccount } from './getapi/getapi.service';
import { GlobalService } from './global.service';
@NgModule({
  imports: [
    CommonModule,
    routing
  ],
  declarations: [Baraproject],
  providers: [
        GetAccount,
        GlobalService
    ],
})
export class BaraprojectModule { }
