import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-baraproject',
  template: `<router-outlet></router-outlet>`,
  styleUrls: ['./baraproject.component.css']
})
export class Baraproject implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
