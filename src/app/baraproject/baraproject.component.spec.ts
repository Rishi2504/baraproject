import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BaraprojectComponent } from './baraproject.component';

describe('BaraprojectComponent', () => {
  let component: BaraprojectComponent;
  let fixture: ComponentFixture<BaraprojectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BaraprojectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BaraprojectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
