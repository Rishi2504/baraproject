/*******************************************************************************
 * Put Server and Plugins configs here
 * ENV: Development
 ******************************************************************************/

'use strict';

const path = require('path');

module.exports = {
    env: 'development',
    server: {
        host: '127.0.0.1',
        port: 6001
    },
    product: {
        name: 'Baraproject Web'
    },
    apiUrl: 'http://www.baraproject.nl/api/', 
    
};
